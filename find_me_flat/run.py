import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
import crawler
import re


class Handler:
    @staticmethod
    def submit_data(widget):
        can_go = True
        list_places_id = ['bienczyce', 'bronowice', 'czyzyny', 'debniki', 'grzegorzki', 'krowodrza',
                          'nowa_huta', 'podgorze', 'pradnik_bialy', 'pradnik_czerwony', 'stare_miasto']
        list_places_names = ['Kraków, Bieńczyce', 'Kraków, Bronowice', 'Kraków, Czyżyny', 'Kraków, Dębniki', 'Kraków, Grzegórzki', 'Kraków, Krowodrza',
                             'Kraków, Nowa Huta', 'Kraków, Podgórze', 'Kraków, Prądnik Biały', 'Kraków, Prądnik Czerwony', 'Kraków, Stare Miasto']

        mail = builder.get_object("mail").get_text()
        match = re.match('^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$', mail)
        price = builder.get_object("price").get_text()
        rooms = builder.get_object("rooms").get_text()
        area = builder.get_object("area").get_text()

        try:
            price = int(price)
            rooms = int(rooms)
            area = int(area)
        except ValueError:
            can_go = False
            print("Price, rooms and msquare have to be a number.")

        places = []
        for i in range(len(list_places_id)):
            place = builder.get_object(list_places_id[i]).get_active()
            if place:
                places.append(list_places_names[i])

        if can_go:
            if mail == '' or price == '' or rooms == '' or area == '' or len(places) == 0:
                print("You didn't pass some data.")
            elif match is None:
                print("E-mail is not valid.")
            else:
                crwlr = crawler.Crawler(mail, price, places, area, rooms)
                crwlr.seek()

        Gtk.main_quit()


builder = Gtk.Builder()
builder.add_from_file("flat-seeker.glade")
builder.connect_signals(Handler())


class GUI:
    def __init__(self):
        _window = builder.get_object("window1")
        _window.connect("delete-event", Gtk.main_quit)
        _window.show_all()

        Gtk.main()


GUI()
