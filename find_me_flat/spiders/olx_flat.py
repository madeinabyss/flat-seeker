import scrapy
from scrapy.crawler import CrawlerProcess


class OlxFlat(scrapy.Spider):
    name = "olxflat"
    allowed_domains = ["olx.pl"]
    start_urls = ['https://www.olx.pl/nieruchomosci/mieszkania/wynajem/krakow/']

    def parse(self, response):
        for offer in response.css('td.offer'):
            if 'olx.pl' in offer.css('div.space.rel > h3 > a::attr(href)').extract_first():
                item = {
                    'Title': offer.css('div.space.rel strong::text').extract_first().strip(),
                    'Place': offer.css('p.color-9.lheight16.marginbott5 > small.breadcrumb > span::text')
                        .extract_first().strip(),
                    'Price': offer.css('p.price strong::text').extract_first().strip()
                }
                yield item

                url = offer.css('div.space.rel > h3 > a::attr(href)').extract_first()
                yield scrapy.Request(url=url, callback=self.parse_details)

        details_urls = response.css('div.space > h3 > a::attr(href)').extract()
        for url in details_urls:
            if 'olx.pl' in url:
                yield scrapy.Request(url=url, callback=self.parse_details)

            next_page_url = response.css('span.fbold.next.abs.large > a::attr(href)').extract_first()
            yield scrapy.Request(url=next_page_url, callback=self.parse)
    @staticmethod
    def parse_details(response):
        details_a = response.css('td.value > strong > a::text').extract()
        details_strong = response.css('td.value > strong::text').extract()

        if details_a[1].strip().isdigit() or details_a[1].strip() == 'Parter':
            yield {
                'Link': response.request.url,
                'Title': response.css('div.offer-titlebox h1::text').extract_first().strip(),
                'Price': response.css('div.price-label strong::text').extract_first().strip(),
                'Type of building': details_a[3].strip(),
                'Level': details_a[1].strip(),
                'No. of rooms': details_a[4].strip(),
                'Who offers': details_a[0].strip(),
                'Furnished': details_a[2].strip(),
                'Area': details_strong[8].strip(),
                'Additional rent': details_strong[11].strip()
            }
        else:
            yield {
                'Link': response.request.url,
                'Title': response.css('div.offer-titlebox h1::text').extract_first().strip(),
                'Price': response.css('div.price-label strong::text').extract_first().strip(),
                'Type of building': details_a[2].strip(),
                'Level': 'No info',
                'No. of rooms': details_a[3].strip(),
                'Who offers': details_a[0].strip(),
                'Furnished': details_a[1].strip(),
                'Area': details_strong[6].strip(),
                'Additional rent': details_strong[9].strip()
            }
