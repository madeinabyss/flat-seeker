import json
import smtplib
from email.message import EmailMessage
from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings
from functools import reduce
import flat_exceptions


class Crawler:
    def __init__(self, mail_to, price, places, area, rooms):
        self._price_condition = price
        self._place_condition = places
        self._area_condition = area
        self._rooms_condition = rooms
        self._mail_to = mail_to
        
    def seek(self):
        # clear file content
        open('./olx.json', 'w').close()

        process = CrawlerProcess(get_project_settings())

        # 'followall' is the name of one of the spiders of the project.
        process.crawl('olxflat', domain='olx.pl')
        process.start()  # the script will block here until the crawling is finished

        data = json.load(open('./olx.json'))  # load scraped data

        out_data = []  # container for data that meets the requirements

        # merge dictionaries with overall and detail data
        # change strings with numbers to integers
        # check conditions and store all data that fits
        for my_dict in data:
            for my_dict_check in data:
                if my_dict != my_dict_check and my_dict_check['Title'] == my_dict['Title'] and \
                        my_dict_check['Price'] == my_dict['Price']:

                    if {'Price', 'No. of rooms', 'Area'}.issubset(my_dict_check):
                        my_dict_check['Price'] = str(my_dict_check['Price'])
                        my_dict_check['No. of rooms'] = str(my_dict_check['No. of rooms'])
                        my_dict_check['Area'] = str(my_dict_check['Area'])
                        price_n = [s for s in my_dict_check['Price'].split() if s.isdigit()]
                        price_s = reduce(lambda x, y: x + str(y), price_n, '')
                        if len(price_s) == 2:
                            price_s = price_s + '00'
                        try:
                            my_dict_check['Price'] = int(price_s)
                        except ValueError:
                            print("Someone provided not valid data. Price for this flat will be changed to 1.")
                            my_dict_check['Price'] = 1

                        rooms_n = [s for s in my_dict_check['No. of rooms'].split() if s.isdigit()]
                        rooms_s = reduce(lambda x, y: x + str(y), rooms_n, '')
                        try:
                            my_dict_check['No. of rooms'] = int(rooms_s)
                        except ValueError:
                            print("Someone provided not valid data. No. of rooms for this flat will be changed to 69.")
                            my_dict_check['No. of rooms'] = 69

                        area_n = [s for s in my_dict_check['Area'].split() if s.isdigit()]
                        area_s = reduce(lambda x, y: x + str(y), area_n, '')
                        if len(area_s) == 1:
                            area_s = area_s + '0'
                        try:
                            my_dict_check['Area'] = int(area_s)
                        except ValueError:
                            print("Someone provided not valid data. Area for this flat will be changed to 666.")
                            my_dict_check['Area'] = 666

                        result_dict = {**my_dict, **my_dict_check}
                        #print(result_dict)
                        if {'Price', 'Place', 'No. of rooms', 'Area'}.issubset(result_dict):
                            if result_dict not in out_data and \
                                    result_dict['Price'] <= self._price_condition and \
                                    result_dict['Place'] in self._place_condition and \
                                    result_dict['No. of rooms'] >= self._rooms_condition and \
                                    result_dict['Area'] >= self._area_condition:
                                out_data.append(result_dict)
                        break

        if len(out_data) == 0:
            raise flat_exceptions.EmptyData

        body = ""

        for iter_dict in out_data:
            for name, detail in sorted(iter_dict.items()):
                body = body + '\n{}: {}'.format(name, detail)
            body = body + '\n'

        mail_from = 'abitbitter@hotmail.com'  # change it
        password = 'doyouwannadatemyavatar?'  # change it

        print(body)
        msg = EmailMessage()
        msg.set_content(body)
        msg['Subject'] = 'Mieszkania spelniajace wymagania'
        msg['From'] = mail_from
        msg['To'] = self._mail_to

        s = smtplib.SMTP(host='smtp-mail.outlook.com', port=587) #change it for your properties
        s.starttls()
        s.login(mail_from, password)
        s.send_message(msg)
        s.quit()

        with open('out_data.json', 'w') as outfile:
            json.dump(out_data, outfile, indent=2)
