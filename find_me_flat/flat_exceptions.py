import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk


class EmptyData(Exception):
    def __init__(self):
        print("Exception: There's no entry that matches your criteria.")
        Gtk.main_quit()
        exit()

    def __call__(self, *args, **kwargs):
        pass
