# Flat Seeker - easy way to find a flat in Cracow.

## WARNING! OLX changed html structure of its site what tends to problems with this program. Within a few days I will try to fix the problem. 

This program goes through olx and finds flats for rent that meet the requirements. 
Flat Seeker comes with GUI, so it's easy to use. Just fill out the fields and click "Submit". 
All flats that fulfil the criteria will be sent to you by e-mail.

Usage:
Run run.py to start a program.
Fill out the fields and click "Submit".